#include <linux/module.h>


struct syscall_vector {
	int id;
	struct syscall_vector *next;
	struct new_syscall *new_sys_calls;
	int rc;
	struct module *owner;
	char *name;
};


struct new_syscall {
	int syscall_num;
	void *syscall_addr;
	struct new_syscall *next;
};


struct syscall_vector *unregister_syscall_vector
			(struct syscall_vector *vector_addr);
int register_syscall_vector(struct syscall_vector *vector_addr);
int num_vectors(void);
struct syscall_vector *get_syscall_vector(int v_id);
void *get_func_ptr(struct new_syscall *vec, int syscall_num);
struct syscall_vector *get_syscall_vector_head(void);
struct syscall_vector *get_syscall_vector_name(char *name);
void print_new_syscalls(int v_id);
