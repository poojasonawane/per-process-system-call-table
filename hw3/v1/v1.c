#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/syscalls.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/register.h>
#include <linux/fs.h>

#define buff_size 4096
struct syscall_vector *vec;
struct new_syscall *create_vector(int, void *);
struct new_syscall *vec_addr;

typedef asmlinkage long (*unlink_ptr)(const char __user *pathname);
typedef asmlinkage long (*open_ptr)(const char __user *filename, int flags, umode_t mode);

asmlinkage long (*open_orig)(const char __user *filename, int flags, umode_t mode);
asmlinkage long open_override(const char __user *filename, int flags, umode_t mode);

asmlinkage long (*unlink_orig)(const char __user *pathname);
asmlinkage long unlink_override(const char __user *pathname);
/*
extern int register_syscall_vector(struct syscall_vector *vector_addr);
extern void *get_syscall_addr(struct new_syscall *head, int syscall_num);
*/
extern struct new_syscall *orig_head;
char *pattern = "virus";
/*
extern void *orig_syscalls[];
vim include/linux/syscalls.h
*/
asmlinkage long open_override(const char __user *filename, int flags, umode_t mode)
{

	long err;
	open_ptr open_orig;
	struct file *filp;
	char *buff;
	int read_size;
	char *ret;
	mm_segment_t oldfs;

	oldfs = get_fs();
	set_fs(KERNEL_DS);

	buff = kmalloc(4096, GFP_KERNEL);

	printk("file : %s\n", filename);
	open_orig = get_func_ptr(orig_head, __NR_open);
	err = open_orig(filename, flags, mode);
	if (err < 0) {
		goto out;
	}


	filp = filp_open(filename, flags, mode);
	if (filp == NULL || IS_ERR(filp)) {
		printk("\tcant do filp open\n");
		err = PTR_ERR(filp);
		goto out;
	}
	printk("\tfilp open sucess\n");
	buff = kmalloc(buff_size, GFP_KERNEL);
	if (unlikely(!buff)) {
		err = -ENOMEM;
		goto out;
	}
	read_size = vfs_read(filp, buff, buff_size, &filp->f_pos);
	set_fs(oldfs);
	ret = strstr(buff, pattern);
	if (ret) {
		err = -EACCES;
		filp_close(filp, NULL);
		goto out;
	}
	/*unlink_orig = get_syscall_addr(orig_head, __NR_unlink);*/
	open_orig = get_func_ptr(orig_head, __NR_open);

	err = open_orig(filename, flags, mode);
out:
	return err;
}

asmlinkage long unlink_override(const char __user *pathname)
{
	long err;
	char *extension;
	/*char *temp_name; */
	int extension_len;
	int pathname_len;
	unlink_ptr unlink_orig;
	err = 0;
	extension = ".protect";
	extension_len = strlen(extension);
	pathname_len = strlen(pathname);
	/*temp_name = kmalloc(sizeof(char)*extension_len, GFP_KERNEL);
	copy_from_user(temp_name, pathname, path_name + )*/
	if (pathname_len > extension_len && !strcmp(pathname + pathname_len - extension_len, extension)) {
		printk("cant remove .protection file\n");
		err = -EPERM;
		goto out;
	}
	/*unlink_orig = get_syscall_addr(orig_head, __NR_unlink);
	printk("before get_func_ptr : inside unlink_override\n");*/
	unlink_orig = get_func_ptr(orig_head, __NR_unlink);

	err = unlink_orig(pathname);
out:
	return err;
}

struct new_syscall *create_vector(int syscall_num, void *addr)
{
	struct new_syscall *s_node1;

	s_node1 = kmalloc(sizeof(struct new_syscall), GFP_KERNEL);
	printk("create_vector function \n");
	if (IS_ERR(s_node1)) {
		printk("Error during kmalloc\n");
		return s_node1;
	}
	s_node1->next = NULL;
	s_node1->syscall_num = syscall_num;
	printk("\tsyscall number : %d\n", __NR_open);
	s_node1->syscall_addr = addr;
	return s_node1;
}

static int __init v1_start(void)
{
	int err = 0;

	struct new_syscall *vec_addr2;
	/* new syscall vector creation */
	vec = kmalloc(sizeof(struct syscall_vector), GFP_KERNEL);
	if (IS_ERR(vec)) {
		err = PTR_ERR(vec);
		goto out;
	}
	vec->next = NULL;
	vec->owner = THIS_MODULE;
	vec->rc = 0;
	vec->name = "vector1";

	vec_addr = create_vector(__NR_unlink, unlink_override);
	if (IS_ERR(vec_addr)) {
		err = PTR_ERR(vec_addr);
		goto out;
	}
	vec->new_sys_calls = vec_addr;

	vec_addr2 = create_vector(__NR_open, open_override);
	if (IS_ERR(vec_addr2)) {
		err = PTR_ERR(vec_addr2);
		goto out;
	}
	vec_addr->next = vec_addr2;


	printk("registration : vector 1 new_sys_calls addr : %lu\n", (unsigned long)vec_addr);
	printk("before registering num_nodes : %d\n", num_vectors());
	err = register_syscall_vector(vec);

	printk("after registering num_nodes : %d\n", num_vectors());
	if (err < 0) {
		printk("Can not register v1\n");
		goto out;
	}
	printk("vector id : %d\n", err);
	err = 0;
out:
	return err;
}
static void __exit v1_end(void)
{
	printk("before unregistering num_nodes : %d\n", num_vectors());

	unregister_syscall_vector(vec);
	printk("after unregistering num_nodes : %d\n", num_vectors());
	printk("register module is removed\n");
}
module_init(v1_start);
module_exit(v1_end);

MODULE_LICENSE("GPL");
