#include "../utility.h"
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#define CLONE_SYSCALLS 0x00001000
#define __NR_clone2 329

/*
 * INPUTS : vector id
 */

int main(int argc, char *argv[]) {
	int ret;
	int curr_pid;
	int cid;

	curr_pid = getpid();
	ret = set_vector_id(curr_pid, atoi(argv[optind]));
	cid = syscall(__NR_clone2,CLONE_SYSCALLS,NULL,NULL,NULL,0,1);
	
	if (cid == 0)
                printf("syscall returned %d\n", cid);
        else
                printf("syscall returned %d (errno=%d)\n", cid, errno);
	
	printf("Message : %s\n", strerror(errno));
	return ret;
}
