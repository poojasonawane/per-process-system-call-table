/*
 * Prints vector info of a given pid 
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include "../ioctl_module/ioctl_header.h"
#define MAX_FILENAME 512
/*
 * printing information
 */
void print_info(char *buffer)
{
	struct info_packet *verify_packet;
	char *name;
	int s;
	verify_packet = (struct info_packet *)malloc(sizeof(struct info_packet));
	memcpy(verify_packet, buffer, sizeof(struct info_packet));
	s = verify_packet->info_size;
	verify_packet = (struct info_packet *)malloc(sizeof(struct info_packet)+ s);
	memcpy(verify_packet, buffer, sizeof(struct info_packet)+s);
	name = (char *)malloc(s);
	memset(name, '\0', s);
	memcpy(name, verify_packet->info_vname, s);
	printf("Vector Id: %d\n", verify_packet->info_vid);
	printf("Vector Name: %s\n", name);
	printf("Vector Reference Count: %d\n", verify_packet->info_rc);

}
int main(int argc, char *argv[])
{
	char *fname;
	char *device_fpath = "/dev/ioctl_device";
	int fd;
	int ret;
	char *p;
	struct ioctl_packet *packet;
	fname = (char*)malloc(MAX_FILENAME);
	printf("Get parent PID: %d\n", getpid());
	memset(fname, 0, MAX_FILENAME);
	memcpy(fname, device_fpath, strlen(device_fpath));
	packet = (struct ioctl_packet *)malloc(sizeof(struct ioctl_packet));
	packet->pid = atoi(argv[optind]);
	packet->vid = 0;
 
	printf("Arguments: %d, %d\n", packet->pid, packet->vid);
	fd = open(fname, 0);
	if (fd < 0) {
		printf("Unable to open character device file\n");
		goto out;
	}
	
	ret = ioctl(fd, IOCTL_SHOW_VECTORS, packet);
	if (ret < 0) {
		printf("IOCTL_SHOW_VECTOR failed!\n");
	}
	//printf("Packet size %d\n", packet->size);
	print_info(packet->buff);
out:
	if (fd > 0)
		close(fd);
	free(fname);
}
