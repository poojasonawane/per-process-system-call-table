/*
 * This program tests for encryption and decryption
 * Overriden syscalls read and write
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "../ioctl_module/ioctl_header.h"
#define MAX_FILENAME 512
int main(int argc, char *argv[])
{
	char *fname;
	char *device_fpath = "/dev/ioctl_device";
	int child = -1;
	int fd;
	int t_fd=0 , t_fd2 =0;
	int ret;
	char *input;
	char *content, *contentr;
	// Encrypting string
	content = "hello world";
	struct ioctl_packet *packet;
	fname = (char*)malloc(MAX_FILENAME);
	//printf("Get parent PID: %d\n", getpid());
	memset(fname, 0, MAX_FILENAME);
	memcpy(fname, device_fpath, strlen(device_fpath));
	packet = (struct ioctl_packet *)malloc(sizeof(struct ioctl_packet));
	packet->pid = getpid();
	packet->vid = atoi(argv[optind]);
	input = argv[optind+1];
	//printf("Enter text to be written: \n");
	
	//printf("Arguments: %d, %d\n", packet->pid, packet->vid);
	fd = open(fname, 0);
	if (fd < 0) {
		printf("Unable to open character device file!\n");
		goto out;
	}
	//printf("Setting the vector id of a process\n");	
	ret = ioctl(fd, IOCTL_SET_VECTOR, packet);
	if (ret < 0) {
		printf("IOCTL_SET_VECTOR failed!\n");
		goto out;
	}
	printf("IOCTL_SET_VECTOR success!\n");
	
	t_fd = open(input, O_WRONLY | O_CREAT);
	//t_fd = open(input, O_RDWR | O_CREAT);

	if (t_fd < 0) {
		perror("Error:\n");
		goto out;
	}
	//printf("content: %s, len: %d\n", content, strlen(content));
	ret = write(t_fd, content, strlen(content));
	//printf("No of bytes written: %d\n", ret);
	if (ret < 0) {
		perror("Error:\n");
		goto out;
	}
	close(t_fd);
	t_fd2 = open(input, O_RDONLY);
	if (t_fd2 < 0) {
                perror("Error:\n");
                goto out;
        }
	//printf("file read open success\n");
	contentr = (char *)malloc(ret);
	ret = read(t_fd2, contentr, strlen(content));
	if (ret < 0) {
		perror("Error:\n");
		goto out;
	}
	printf("Decrypted message: %s\n", contentr);
	return 0;
out:
	if (fd > 0)
		close(fd);
	if (t_fd > 0)
		close(t_fd);
	if (t_fd2 > 0)
		close(t_fd2);
	free(fname);
}
