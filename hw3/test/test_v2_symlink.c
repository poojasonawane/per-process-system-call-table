#include "../utility.h"
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>

/*
 *INPUTS : vector id, filename, symlink
 */
int main(int argc, char *argv[]) {
	int ret;
	int curr_pid;
	int vec_id;
	char *filename, *symlink;
	curr_pid = getpid();
	printf("**********************************\n");
	vec_id =  atoi(argv[optind]);
	ret = set_vector_id(curr_pid, vec_id);
	printf("vec id : %d \n",vec_id );
	filename = argv[optind + 1];
	symlink = argv[optind + 2];
	printf(" Filename : %s , symlink : %s \n",filename,symlink);
	syscall(__NR_symlink, filename, symlink);
	printf("Message : %s\n", strerror(errno));
	printf("**********************************\n");
	return ret;
}
