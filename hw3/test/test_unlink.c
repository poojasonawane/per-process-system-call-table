#include "../utility.h"
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
//TODO : get vector id by name in userland

int main(int argc, char *argv[]) {
	int ret;
	int curr_pid;
		
	curr_pid = getpid();
	ret = set_vector_id(curr_pid, atoi(argv[1]));
	syscall(__NR_unlink, argv[2]);
	//open("file_name", O_RDONLY, S_IRWXU);
	printf("Message: %s\n", strerror(errno) );
	return ret;
}
