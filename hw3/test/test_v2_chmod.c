#include "../utility.h"
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>

/*
 *INPUTS : vector id , filename , mode
 */
int main(int argc, char *argv[]) {
	int ret;
	int curr_pid;
	FILE *fp;
	int md;
	char *filename;
	printf("********************************************\n");
	curr_pid = getpid();
	ret = set_vector_id(curr_pid, atoi(argv[optind]));
	filename = argv[optind + 1];
	md = strtol(argv[optind + 2], 0, 8);
	syscall(__NR_chmod, filename, md);
	printf("Message from syscall : %s\n", strerror(errno));
	printf("********************************************\n");
	return ret;
}
