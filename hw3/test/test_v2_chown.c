#include "../utility.h"
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>
#include <pwd.h>

/*
 *INPUTS : vector id,username,path
 */
int main(int argc, char *argv[]) {
	uid_t uid;
	int ret;
	int curr_pid;
	FILE *fp;
	struct passwd *pwd;
	char *username, *path;

	curr_pid = getpid();
	printf("*****************************************\n");
	ret = set_vector_id(curr_pid, atoi(argv[optind]));	
	username = argv[optind + 1];
	pwd = getpwnam(username);
	if (pwd == NULL){
		printf("Cannot retrieve UID for the given user \n");
		return -1;
	}
	path = argv[optind + 2];
	syscall(__NR_chown,path,uid,-1);
	printf("Message from syscall : %s\n", strerror(errno));
	printf("*****************************************\n");
	return ret;
}
