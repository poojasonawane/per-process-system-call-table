#include "../utility.h"
#include <stdio.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>

/*
 *Inputs : Vector id of Vector 2 , PID of process to be killed ;
 */
int main(int argc, char *argv[]) {
	int curr_pid;
        int child = -1;
        int ret;
		
	curr_pid = getpid();
	printf("******************************************\n");
	ret = set_vector_id(curr_pid,atoi(argv[optind]));

	syscall(__NR_kill, atoi(argv[optind + 1]), SIGKILL);
	printf("Message : %s\n", strerror(errno));
	printf("******************************************\n");
	return ret;
}
