/*
 * This program tests overridden mkdir
 * If, a.out process, then it cannot create a directory
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "../ioctl_module/ioctl_header.h"
#define MAX_FILENAME 512
int main(int argc, char *argv[])
{
	char *fname;
	char *device_fpath = "/dev/ioctl_device";
	int child = -1;
	int fd;
	int ret;
	char *p;
	struct ioctl_packet *packet;
	char *dir;
	mode_t mask;
	mask = umask(mask);
	fname = (char*)malloc(MAX_FILENAME);
	//printf("Get parent PID: %d\n", getpid());
	memset(fname, 0, MAX_FILENAME);
	memcpy(fname, device_fpath, strlen(device_fpath));
	packet = (struct ioctl_packet *)malloc(sizeof(struct ioctl_packet));
	packet->pid = getpid();
	packet->vid = atoi(argv[optind]);
	dir = argv[optind + 1];
	//printf("Arguments: %d, %d\n", packet->pid, packet->vid);
	fd = open(fname, 0);
	if (fd < 0) {
		printf("Unable to open character device file\n");
		goto out;
	}
	printf("Setting the Vector Id of a process\n");	
	ret = ioctl(fd, IOCTL_SET_VECTOR, packet);
	if (ret < 0) {
		printf("IOCTL_SET_VECTOR failed!\n");
		goto out;
	}
	printf("IOCTL_SET_VECTOR success!\n");
	//printf("New directory created: %s\n", dir);
	ret = mkdir(dir, 0777);
	if (ret < 0) {
		printf("Message: %s\n", strerror(errno));
		goto out;
	}
	return 0;
out:
	if (fd > 0)
		close(fd);
	free(fname);
}
