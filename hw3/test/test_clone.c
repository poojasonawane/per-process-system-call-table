/*
 * When a process clones, child will inherit the syscall vector from its parent based on CLONE_SYSCALLS.
 */
#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sched.h>
#include <signal.h>
#include <sys/wait.h>

#include "../ioctl_module/ioctl_header.h"
#define MAX_FILENAME 512
#define CLONE_SYSCALLS          0x00001000

/*
 * printing information
 */

clone_body(void *arg) {
        int ret1 = 0;
        //printf("In child process\n");
        //ret1 = open("test_clone", 66, 77);
        //printf("child return value is: %d\n\n", ret1);
        _exit(0);
}


void print_info(char *buffer)
{
	struct info_packet *verify_packet;
	char *name;
	int s;
	verify_packet = (struct info_packet *)malloc(sizeof(struct info_packet));
	memcpy(verify_packet, buffer, sizeof(struct info_packet));
	s = verify_packet->info_size;
	verify_packet = (struct info_packet *)malloc(sizeof(struct info_packet)+ s);
	
	memcpy(verify_packet, buffer, sizeof(struct info_packet)+s);
	name = (char *)malloc(s);
	memset(name, '\0', s);
	memcpy(name, verify_packet->info_vname, s);
	printf("Vector Id: %d\n", verify_packet->info_vid);
	printf("Vector Name: %s\n", name);
	printf("Vector Reference Count: %d\n", verify_packet->info_rc);
	printf("*******************************\n");
		
}
int main(int argc, char *argv[])
{
	char *fname;
	char *device_fpath = "/dev/ioctl_device";
	int child = -1;
	int fd;
	int ret, rc;
	char *p;
	void **stack;
	struct ioctl_packet *packet;
	fname = (char*)malloc(MAX_FILENAME);
	memset(fname, 0, MAX_FILENAME);
	memcpy(fname, device_fpath, strlen(device_fpath));
	packet = (struct ioctl_packet *)malloc(sizeof(struct ioctl_packet));
 
	fd = open(fname, 0);
	if (fd < 0) {
		printf("Unable to open character device file\n");
		goto out;
	}
	packet->vid = 2;
	packet->pid = getpid();
	ret = ioctl(fd, IOCTL_SET_VECTOR, packet);
        if (ret < 0) {
        	printf("IOCTL_SET_VECTOR failed!\n");
        }
	printf("*******************************\n"); 
	printf("Vector Id of parent is: %d\n", packet->vid);
	
	stack = (void **)malloc(65536);
	if(strcmp(argv[1], "y")==0)
		rc = clone(&clone_body, stack+6553, SIGCHLD | CLONE_FILES | CLONE_VM | CLONE_SYSCALLS, NULL);
	else
		rc = clone(&clone_body, stack+6553, SIGCHLD | CLONE_FILES | CLONE_VM, NULL);
	if(rc<0){
		printf("Clone failed\n");
		goto out;
	}
        if(rc){
		packet->pid = getpid();
	        printf("Parent vector info (PID: %d):\n", packet->pid);
        	ret = ioctl(fd, IOCTL_SHOW_VECTORS, packet);
        	if (ret < 0) {
                	printf("IOCTL_SHOW_VECTOR failed!\n");
        	}
        	print_info(packet->buff);
		
		packet->pid = rc;
                printf("Child vector info (PID: %d):\n", packet->pid);
                ret = ioctl(fd, IOCTL_SHOW_VECTORS, packet);
		if (ret == 0) {
                        printf("Using global syscall table!\n");
                } else if (ret < 0) {
                        printf("IOCTL_SHOW_VECTOR failed!\n");
                } else {
                        print_info(packet->buff);
                }

	}
		
	return 0;
out:
	if (fd > 0)
		close(fd);
	free(fname);

}
