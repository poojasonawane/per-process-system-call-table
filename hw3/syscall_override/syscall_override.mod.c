#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xc5f6552c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x84cd853c, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xc6b8a6ef, __VMLINUX_SYMBOL_STR(get_func_ptr) },
	{ 0xeaf93755, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xcd1647c9, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x2c7b47d1, __VMLINUX_SYMBOL_STR(pv_cpu_ops) },
	{ 0xb6d7823f, __VMLINUX_SYMBOL_STR(get_syscall_vector) },
	{ 0xdcb0349b, __VMLINUX_SYMBOL_STR(sys_close) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";


MODULE_INFO(srcversion, "3D98FA00CBC57E9BD756071");
