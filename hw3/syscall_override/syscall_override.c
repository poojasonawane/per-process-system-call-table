#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/syscalls.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/register.h>

typedef asmlinkage long (*unlink_ptr)(const char __user *pathname);
typedef asmlinkage long (*mkdir_ptr)(const char __user *pathname, umode_t mode);
typedef asmlinkage long (*chdir_ptr)(const char __user *filename);
typedef asmlinkage long (*open_ptr)(const char __user *filename, int flags, umode_t mode);
typedef asmlinkage int (*kill_ptr)(pid_t, int);
typedef asmlinkage long (*read_ptr) (unsigned int fd, char __user *buf, size_t count);
typedef asmlinkage long(*clone2_ptr)(unsigned long, void*, int *, int *, unsigned long, int);
typedef asmlinkage int (*symlink_ptr)(const char *, const char *);
typedef asmlinkage int (*chmod_ptr)(const char*, mode_t);
typedef asmlinkage int (*chown_ptr)(const char *, uid_t, gid_t);

/*typedef asmlinkage long (*read_ptr) (unsigned int fd, char __user *buf, size_t count);*/

typedef asmlinkage long (*write_ptr)(unsigned int fd, char __user *buf, size_t count);
unlink_ptr unlink_orig;
mkdir_ptr mkdir_orig;
chdir_ptr chdir_orig;
open_ptr open_orig;
kill_ptr kill_orig;
read_ptr read_orig;
write_ptr write_orig;
clone2_ptr clone2_orig;
symlink_ptr symlink_orig;
chmod_ptr chmod_orig;
chown_ptr chown_orig;


unsigned long **sys_call_tbl_addr;
unsigned long original_cr0;
void *get_func_ptr(struct new_syscall *vec, int syscall_num);
void *orig_syscalls[350];
struct new_syscall *orig_head;
struct new_syscall *orig_tail;

EXPORT_SYMBOL(orig_head);
EXPORT_SYMBOL(orig_syscalls);

int add_node_to_orig(void *orig_syscall_addr, int syscall_num)
{
	struct new_syscall *node;
	/* struct new_syscall *temp; */
	int err = 0;

	node = kmalloc(sizeof(struct new_syscall), GFP_KERNEL);
	if (IS_ERR(node)) {
		printk("Can not malloc \n");
		err = PTR_ERR(node);
		goto out;
	}
	node->syscall_num = syscall_num;
	node->next = NULL;
	node->syscall_addr = orig_syscall_addr;

	/* no node in list */
	if (!orig_head) {
		orig_head = node;
		orig_tail = node;
	} else {
		orig_tail->next = node;
		orig_tail = node;
	}

out:
	return err;
}

void *getsyscall_address(int syscall_no, int v_id)
{
	struct syscall_vector *curr_vec;
	void *add;

	curr_vec = get_syscall_vector(v_id);
	if (!curr_vec) {
		printk("system call vector: %d not present\n", v_id);
		add = NULL;
		goto out;
	}
	add =  get_func_ptr(curr_vec->new_sys_calls, syscall_no);
	if (!add) {
		printk("syscall address for %d in %d vector is NULL\n", syscall_no, v_id);
		goto out;
	}
out:
	return add;
}

asmlinkage long clone2_redirect(unsigned long flags, void *child_stack, int *ptid, int *ctid, unsigned long newtls, int vec_id)
{
	clone2_ptr new_clone2;
	long err;
	unsigned long v_id;

	err = 0;
	v_id = current->vector_id;
	if (v_id == 0) {
		err = clone2_orig(flags, child_stack, ptid, ctid, newtls, vec_id);
		goto out;
	}

	printk("Inside clone2 redirect \n");
	new_clone2 = (clone2_ptr)getsyscall_address(__NR_clone2, v_id);
	if (!new_clone2) {
		printk("clone2() syscall address is NULL for vector %lu \n", v_id);
		goto out;
	}

	err = new_clone2(flags, child_stack, ptid, ctid, newtls, vec_id);
out:
	return err;

}

asmlinkage int kill_redirect(pid_t pid, int sig)
{
	int err;
	unsigned long v_id;
	kill_ptr new_kill;

	err = 0;
	v_id = current->vector_id;
	if (v_id == 0) {
		err = kill_orig(pid, sig);
		goto out;
	}

	printk("Inside kill redirect \n");
	new_kill = (kill_ptr)getsyscall_address(__NR_kill, v_id);
	if (!new_kill) {
		printk("kill() syscall address is NULL for vector %lu \n", v_id);
		goto out;
	}

	err = new_kill(pid, sig);
out:
	return err;
}

asmlinkage int chown_redirect(const char *pathname, uid_t owner, gid_t group)
{
	int err;
	unsigned long v_id;
	chown_ptr new_chown;

	err = 0;
	v_id = current->vector_id;
	if (v_id == 0) {
		err = chown_orig(pathname, owner, group);
		goto out;
	}

	printk("Inside chown redirect \n");
	new_chown = (chown_ptr)getsyscall_address(__NR_chown, v_id);
	if (!new_chown) {
		printk("chown() syscall address is NULL for vector %lu \n", v_id);
		goto out;
	}
	err = new_chown(pathname, owner, group);
out:
	return err;
}


asmlinkage int symlink_redirect(const char *name1, const char *name2)
{
	int err;
	unsigned long v_id;
	symlink_ptr new_symlink;

	err = 0;
	v_id = current->vector_id;
	if (v_id == 0) {
		err = symlink_orig(name1, name2);
		goto out;
	}

	printk("Inside symlink redirect \n");
	new_symlink = (symlink_ptr)getsyscall_address(__NR_symlink, v_id);
	if (!new_symlink) {
		printk("symlink() syscall address is NULL for vector %lu \n", v_id);
		goto out;
	}

	err = new_symlink(name1, name2);
out:
	return err;
}

asmlinkage int chmod_redirect(const char *pathname, mode_t mode)
{
	int err;
	unsigned long v_id;
	chmod_ptr new_chmod;

	err = 0;
	v_id = current->vector_id;
	if (v_id == 0) {
		err = chmod_orig(pathname, mode);
		goto out;
	}

	printk("Inside chmod redirect \n");
	new_chmod = (chmod_ptr)getsyscall_address(__NR_chmod, v_id);
	if (!new_chmod) {
		printk("chmod() syscall address is NULL for vector %lu \n", v_id);
		goto out;
	}

	err = new_chmod(pathname, mode);
out:
	return err;
}


asmlinkage long unlink_redirect(const char __user *pathname)
{
	long err;
	unsigned long v_id;
	struct syscall_vector *curr_vec;
	unlink_ptr new_unlink;

	err = 0;
	v_id = current->vector_id;
	if (v_id == 0) {
		err = unlink_orig(pathname);
		goto out;
	}
	printk("Inside unlink_override. vector id : %lu\n", v_id);
	curr_vec = get_syscall_vector(v_id);
	if (!curr_vec) {
		printk("system call vector not present\n");
		goto out;
	}
	new_unlink = (unlink_ptr) get_func_ptr(curr_vec->new_sys_calls, __NR_unlink);
	if (!new_unlink) {
		printk("function address is NULL\n");
		err = unlink_orig(pathname);
		goto out;
	}
	printk("executing new unlink function \n");
	err = new_unlink(pathname);
out:
	return err;
}

asmlinkage long open_redirect(const char __user *filename, int flags, umode_t mode)
{
	long err;
	unsigned long v_id;
	struct syscall_vector *curr_vec;
	open_ptr new_open;
	//printk("Inside open_redirect \n");

	err = 0;
	v_id = current->vector_id;
	if (v_id == 0) {
		err = open_orig(filename, flags, mode);
		goto out;
	}
	printk("\tvector id : %lu\n", v_id);
	curr_vec = get_syscall_vector(v_id);
	if (!curr_vec) {
		printk("\tsystem call vector not present\n");
		goto out;
	}
	printk("\tcurr_vec id : %d\n", curr_vec->id);
	printk("\tcurr_vec new_sys_calls addr : %lu\n", (unsigned long)curr_vec->new_sys_calls);
	new_open = (open_ptr) get_func_ptr(curr_vec->new_sys_calls, __NR_open);
	if (!new_open) {
		printk("\tfunction address is NULL\n");
		err = open_orig(filename, flags, mode);
		goto out;
	}
	printk("\texecuting new open function \n");
	err = new_open(filename, flags, mode);
out:
	return err;
}

asmlinkage long mkdir_override(const char __user *pathname, umode_t mode)
{
	long err;
	unsigned long v_id;
	struct syscall_vector *curr_vec;
	mkdir_ptr new_mkdir;
	err = 0;
	v_id = current->vector_id;

	if (v_id == 0) {
		err = mkdir_orig(pathname, mode);
		goto out;
	}
	printk("Inside mkdir_override. Vector Id : %lu\n", v_id);
	curr_vec = get_syscall_vector(v_id);
	if (!curr_vec) {
		printk("System call vector not present\n");
		goto out;
	}
	new_mkdir = (mkdir_ptr) get_func_ptr(curr_vec->new_sys_calls, __NR_mkdir);
	if (!new_mkdir) {
		pr_info("Function address of new mkdir is NULL\n");
		err = mkdir_orig(pathname, mode);
		goto out;
	}
	printk("Executing new mkdir function \n");
	err = new_mkdir(pathname, mode);
out:
	return err;

}

asmlinkage long write_override(unsigned int fd, char __user *buff, size_t count)
{
	long err;
	unsigned long v_id;
	struct syscall_vector *curr_vec;
	write_ptr new_write;
	err = 0;
	v_id = current->vector_id;

	if (v_id == 0) {
		err = write_orig(fd, buff, count);
		goto out;
	}
	//printk("Inside write_override. Vector Id : %lu\n", v_id);
	curr_vec = get_syscall_vector(v_id);
	if (!curr_vec) {
		printk("System call vector not present\n");
		goto out;
	}
	new_write = (write_ptr) get_func_ptr(curr_vec->new_sys_calls, __NR_write);
	if (!new_write) {
		pr_info("Function address of new write is NULL\n");
		err = write_orig(fd, buff, count);
		goto out;
	}
	printk("Executing new write function \n");
	err = new_write(fd, buff, count);
out:
	return err;

}

asmlinkage long read_override(unsigned int fd, char __user *buff, size_t count)
{
	long err;
	unsigned long v_id;
	struct syscall_vector *curr_vec;
	read_ptr new_read;
	err = 0;
	v_id = current->vector_id;

	if (v_id == 0) {
		err = read_orig(fd, buff, count);
		goto out;
	}
	printk("Inside read_override. Vector Id : %lu\n", v_id);
	curr_vec = get_syscall_vector(v_id);
	if (!curr_vec) {
		printk("System call vector not present\n");
		goto out;
	}
	new_read = (read_ptr) get_func_ptr(curr_vec->new_sys_calls, __NR_read);
	if (!new_read) {
		pr_info("Function address of new read is NULL\n");
		err = read_orig(fd, buff, count);
		goto out;
	}
	printk("Executing new read function \n");
	err = new_read(fd, buff, count);
out:
	return err;

}
asmlinkage long chdir_override(const char __user *filename)
{
	return chdir_orig(filename);
}

unsigned long **get_syscall_tbl(void)
{
	unsigned long int offset = PAGE_OFFSET;
	unsigned long **sct;
	while (offset < ULLONG_MAX) {
		sct = (unsigned long **)offset;
		if (sct[__NR_close] == (unsigned long *) sys_close)
			return sct;
		offset += sizeof(void *);
	}
	return NULL;
}
EXPORT_SYMBOL(get_syscall_tbl);

static int __init interceptor_start(void)
{
	int err = 0;
	/*if (!(sys_call_tbl_addr = get_syscall_tbl()))
		return -1; */
	sys_call_tbl_addr = get_syscall_tbl();
	if (!sys_call_tbl_addr)
		return -1;
	original_cr0 = read_cr0();
	write_cr0(original_cr0 & ~0x00010000);

	chdir_orig = (void *)sys_call_tbl_addr[__NR_chdir];
	unlink_orig = (void *)sys_call_tbl_addr[__NR_unlink];
	mkdir_orig = (void *)sys_call_tbl_addr[__NR_mkdir];
	open_orig = (void *)sys_call_tbl_addr[__NR_open];
	kill_orig = (void *)sys_call_tbl_addr[__NR_kill];
	read_orig = (void *)sys_call_tbl_addr[__NR_read];
	write_orig = (void *)sys_call_tbl_addr[__NR_write];
	clone2_orig = (void *)sys_call_tbl_addr[__NR_clone2];
	symlink_orig = (void *)sys_call_tbl_addr[__NR_symlink];
	chmod_orig = (void *)sys_call_tbl_addr[__NR_chmod];
	chown_orig = (void *)sys_call_tbl_addr[__NR_chown];


	add_node_to_orig(chdir_orig, __NR_chdir);
	add_node_to_orig(unlink_orig, __NR_unlink);
	add_node_to_orig(mkdir_orig, __NR_mkdir);
	add_node_to_orig(open_orig, __NR_open);
	add_node_to_orig(kill_orig, __NR_kill);
	add_node_to_orig(read_orig, __NR_read);
	add_node_to_orig(write_orig, __NR_write);
	add_node_to_orig(clone2_orig, __NR_clone2);
	add_node_to_orig(symlink_orig, __NR_symlink);
	add_node_to_orig(chmod_orig, __NR_chmod);
	add_node_to_orig(chown_orig, __NR_chown);


	sys_call_tbl_addr[__NR_chdir] = (unsigned long *)chdir_override;
	sys_call_tbl_addr[__NR_unlink] = (unsigned long *)unlink_redirect;
	sys_call_tbl_addr[__NR_mkdir] = (unsigned long *)mkdir_override;
	sys_call_tbl_addr[__NR_open] = (unsigned long *)open_redirect;
	sys_call_tbl_addr[__NR_kill] = (unsigned long *)kill_redirect;
	sys_call_tbl_addr[__NR_read] = (unsigned long *)read_override;
	sys_call_tbl_addr[__NR_write] = (unsigned long *)write_override;
	sys_call_tbl_addr[__NR_clone2] = (unsigned long *)clone2_redirect;
	sys_call_tbl_addr[__NR_symlink] = (unsigned long *)symlink_redirect;
	sys_call_tbl_addr[__NR_chmod] = (unsigned long *)chmod_redirect;
	sys_call_tbl_addr[__NR_chown] = (unsigned long *)chown_redirect;

	write_cr0(original_cr0);
	return err;

}
static void interceptor_end(void)
{
	if (!sys_call_tbl_addr)
		return;
	original_cr0 = read_cr0();
	write_cr0(original_cr0 & ~0x00010000);

	sys_call_tbl_addr[__NR_chdir] = (unsigned long *)chdir_orig;
	sys_call_tbl_addr[__NR_unlink] = (unsigned long *)unlink_orig;
	sys_call_tbl_addr[__NR_mkdir] = (unsigned long *)mkdir_orig;
	sys_call_tbl_addr[__NR_open] = (unsigned long *)open_orig;
	sys_call_tbl_addr[__NR_kill] = (unsigned long *)kill_orig;
	sys_call_tbl_addr[__NR_clone2] = (unsigned long *)clone2_orig;
	sys_call_tbl_addr[__NR_symlink] = (unsigned long *)symlink_orig;
	sys_call_tbl_addr[__NR_chmod] = (unsigned long *)chmod_orig;
	sys_call_tbl_addr[__NR_chown] = (unsigned long *)chown_orig;
	sys_call_tbl_addr[__NR_read] = (unsigned long *)read_orig;
	sys_call_tbl_addr[__NR_write] = (unsigned long *)write_orig;
	write_cr0(original_cr0);
}

static int __init syscall_override_start(void)
{
	int err = 0;
	err = interceptor_start();
	if (err < 0) {
		interceptor_end();
		goto out;
	}
	pr_info("In syscall_override_start\n");
out:
	return err;
}
static void __exit syscall_override_end(void)
{
	interceptor_end();
	pr_info("In syscall_override_end\n");
}
module_init(syscall_override_start);
module_exit(syscall_override_end);

MODULE_LICENSE("GPL");
