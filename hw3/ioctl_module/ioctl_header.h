#define DEVICE_NUM 121
#define DEVICE_NAME "ioctl_device"
#define IOCTL_SHOW_VECTORS _IOR(DEVICE_NUM, 3, unsigned long)
#define IOCTL_SET_VECTOR _IOR(DEVICE_NUM, 1, unsigned long)
#define IOCTL_REMOVE_VECTOR _IOR(DEVICE_NUM, 2, unsigned long)

#define MAX_FILENAME 512
#define SIZE 4096
struct ioctl_packet {
	int pid;
	int vid;
	char buff[SIZE];
	int size;
};

struct info_packet {
	int info_vid;
	int info_rc;
	int info_size;
	char info_vname[1];
};
