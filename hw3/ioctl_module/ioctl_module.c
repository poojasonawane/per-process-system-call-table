#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include "ioctl_header.h"
#include <linux/register.h>
#include <linux/pid.h>
#include <linux/pid_namespace.h>
#include <linux/uaccess.h>
/*
 * This function returns
 * -1 Invalid vid
 * -2 Invalid pid
 *  0 Success
 */
static long device_ioctl(struct file *file, unsigned int cmd,
			unsigned long arg)
{
	long ret = 0;
	int removed_vector_id;
	struct task_struct *task;
	struct pid *pid_struct;
	int disp_vector_id;
	struct ioctl_packet *pack;
	struct info_packet *info;
	int no_of_bytes;
	struct syscall_vector *temp;
	char *vname;
	char *buff_start;
	int size = 0;
	pack = (struct ioctl_packet *)arg;
	pr_info("In device_ioctl\n");
	pr_info("PID is: %d\n", current->pid);
	switch (cmd) {
	case IOCTL_SET_VECTOR:
		pr_info("In IOCTL_SET_VECTOR pid, vid : %d, %d\n", pack->pid, pack->vid);
		pid_struct = find_get_pid(pack->pid);
		if (pid_struct) {
			task = pid_task(pid_struct, PIDTYPE_PID);
			removed_vector_id = task->vector_id;
			pr_info("Before get_syscall_vector\n");
			if (pack->vid != 0 && get_syscall_vector(pack->vid) == NULL) {
				printk("get_syscall_vector failed vector id : %d \n", pack->vid);
				ret = -1;
				goto out;
			}
			pr_info("Vector get_syscall_vector\n");
			task->vector_id = pack->vid;
			pr_info("Removed vector id: %d", removed_vector_id);
			pr_info("Process %d Set to vector id %d", pack->pid, task->vector_id);
			if (removed_vector_id != 0) {
				module_put((get_syscall_vector(removed_vector_id))->owner);
			}
			if (task->vector_id != 0) {
				__module_get((get_syscall_vector(task->vector_id))->owner);
			}
		} else {
			ret = -2;
			goto out;
		}
		break;
	case IOCTL_SHOW_VECTORS:
		if (pack->pid == 0) {
			/* Show all vectors */
			pr_info("In IOCTL_SHOW_VECTORS if cond\n");
			buff_start = pack->buff;
			temp = get_syscall_vector_head();
			while (temp != NULL) {
				pr_info("temp is not null\n");
				vname = temp->name;
				info = kmalloc(sizeof(struct info_packet)+strlen(vname), GFP_KERNEL);
				info->info_vid = temp->id;
				info->info_size = strlen(vname);
				info->info_rc = atomic_read(&temp->owner->refcnt);
				info->info_rc -= 1;

				pr_info("Before memcpy\n");
				memcpy(info->info_vname, vname, strlen(vname));
				pr_info("After memcpy\n");
				no_of_bytes = copy_to_user(buff_start, info, sizeof(struct info_packet)+strlen(vname));
				if (no_of_bytes) {
					pr_info("Not copied properly\n");
				}
				buff_start += sizeof(struct info_packet)+strlen(vname);
				size += sizeof(struct info_packet)+strlen(vname);
				temp = temp->next;
			}
			pr_info("Before copying size size: %d", size);
			no_of_bytes = copy_to_user(&pack->size, &size, sizeof(int));
			if (no_of_bytes) {
				pr_info("size not copied properly\n");
			}
		} else {
			pr_info("In IOCTL_SHOW_VECTORS for pid %d\n", pack->pid);
			pid_struct = find_get_pid(pack->pid);
			if (pid_struct) {
				task = pid_task(pid_struct, PIDTYPE_PID);
				disp_vector_id = task->vector_id;
				if (disp_vector_id == 0) {
					ret = 0;
					goto out;
				}
				temp = get_syscall_vector(disp_vector_id);
				pr_info("Here: Vector ID for process %d is %d\n", pack->pid, disp_vector_id);
				if (temp) {
					vname = temp->name;
					printk("vname : %s\n", vname);
				} else {
					ret = -1;
					goto out;
				}
				info = kmalloc(sizeof(struct info_packet)+strlen(vname), GFP_KERNEL);
				info->info_vid = disp_vector_id;
				info->info_size = strlen(vname);
				info->info_rc = atomic_read(&temp->owner->refcnt);
				info->info_rc -= 1;
				pr_info("Before memcpy\n");
				memcpy(info->info_vname, vname, strlen(vname));
				pr_info("After memcpy\n");
				no_of_bytes = copy_to_user(pack->buff, info, sizeof(struct info_packet)+strlen(vname));
				if (no_of_bytes) {
					pr_info("Bytes not copied :%d\n", no_of_bytes);
				}
				ret = 1;
			} else {
				ret = -2;
				goto out;
			}
		}
		break;
	}
out:
	return ret;
}
struct file_operations fops = {
	.unlocked_ioctl = device_ioctl,
};
static void __exit ioctl_end(void)
{
	unregister_chrdev(DEVICE_NUM, DEVICE_NAME);

}
static int __init ioctl_start(void)
{
	int ret = 0;
	ret = register_chrdev(DEVICE_NUM, DEVICE_NAME, &fops);
	if (ret < 0) {
		pr_info("Failed registering character device\n");
		goto out;
	}
	pr_info("Successfully registered character device\n");
out:
	return ret;
}

module_init(ioctl_start);
module_exit(ioctl_end);

MODULE_LICENSE("GPL");

