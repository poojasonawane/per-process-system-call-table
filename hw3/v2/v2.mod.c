#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xc5f6552c, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x50c06bb0, __VMLINUX_SYMBOL_STR(unregister_syscall_vector) },
	{ 0xa1a5ce0b, __VMLINUX_SYMBOL_STR(vfs_unlink) },
	{ 0x45722d86, __VMLINUX_SYMBOL_STR(filp_close) },
	{ 0xe0757058, __VMLINUX_SYMBOL_STR(register_syscall_vector) },
	{ 0xd81f4ff, __VMLINUX_SYMBOL_STR(num_vectors) },
	{ 0x4437bce1, __VMLINUX_SYMBOL_STR(filp_open) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x6adfcfd0, __VMLINUX_SYMBOL_STR(vfs_write) },
	{ 0xe914e41e, __VMLINUX_SYMBOL_STR(strcpy) },
	{ 0x754d539c, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x61651be, __VMLINUX_SYMBOL_STR(strcat) },
	{ 0x91715312, __VMLINUX_SYMBOL_STR(sprintf) },
	{ 0xcd1647c9, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0x84cd853c, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xc6b8a6ef, __VMLINUX_SYMBOL_STR(get_func_ptr) },
	{ 0x2e9a4639, __VMLINUX_SYMBOL_STR(orig_head) },
	{ 0x34296c4b, __VMLINUX_SYMBOL_STR(pid_vnr) },
	{ 0xdc9caf7d, __VMLINUX_SYMBOL_STR(cpu_tss) },
	{ 0xeaf93755, __VMLINUX_SYMBOL_STR(current_task) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=syscall_override";


MODULE_INFO(srcversion, "D192BA3450E9DDA8A92E67B");
