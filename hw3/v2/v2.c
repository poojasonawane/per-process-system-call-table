#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/syscalls.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/register.h>
#include <linux/uaccess.h>
struct syscall_vector *vec;
struct new_syscall *create_vector(int, void *);
struct new_syscall *vec_addr;
struct file *kill_log;
struct file *clone2_log;
struct file *symlink_log;
struct file *chmod_log;
struct file *chown_log;

asmlinkage long (*kill_orig)(pid_t, int);
asmlinkage long (*clone2_orig)(unsigned long, void*, \
			int *, int *, unsigned long, int);
asmlinkage int (*symlink_orig)(const char *, const char *);
asmlinkage int (*chmod_orig)(const char *, mode_t);
asmlinkage int (*chown_orig)(const char *, uid_t, gid_t);

extern struct new_syscall *orig_head;

asmlinkage long clone2_override(unsigned long flags, void *child_stack, \
			int *ptid, int *ctid, unsigned long newtls, int vid)
{
	pid_t by;
	char *pid1, *com, *errs;
	long err;
	mm_segment_t oldfs;

	by = task_tgid_vnr(current);
	oldfs = get_fs();

	err = 0;
	clone2_orig = get_func_ptr(orig_head, __NR_clone2);

	if (!clone2_orig) {
		printk("\t\tFunction pointer for clone2() is NULL \n");
		goto out;
	}
	err = clone2_orig(flags, child_stack, ptid, ctid, newtls, vid);

	pid1 = kzalloc(64, GFP_KERNEL);
	if (pid1 == NULL) {
		printk("\t\tFailed to allocate buffer in clone2_override\n");
		goto out;
	}

	errs = kzalloc(128, GFP_KERNEL);
	if (errs == NULL) {
		printk("\t\tFailed to allocate buffer in clone2_override\n");
		goto out;
	}


	com = kzalloc(256, GFP_KERNEL);
	if (com == NULL) {
		printk("\t\tFailed to allocate buffer in clone2_override\n");
		goto out;
	}

	sprintf(pid1, "%d", by);
	sprintf(errs, "%ld", err);
	strcat(com, errs);
	strcat(com, ": cloned from :");
	strcat(com, pid1);
	strcat(com, "\n");
	set_fs(KERNEL_DS);
	vfs_write(clone2_log, com, strlen(com), &clone2_log->f_pos);
	set_fs(oldfs);
	if (pid1 != NULL)
		kfree(pid1);
	if (com != NULL)
		kfree(com);

out:
	return err;
}

asmlinkage int chown_override(const char *pathname, uid_t owner, gid_t group)
{
	char *com, *own, *grp, *errs;
	int err;
	mm_segment_t oldfs;
	int l1, tot;

	oldfs = get_fs();
	l1 = 0;
	chown_orig = get_func_ptr(orig_head, __NR_chown);
	err = chown_orig(pathname, owner, group);

	errs = kzalloc(64, GFP_KERNEL);
	if (errs == NULL) {
		printk("Failed to allocate buffer in chown_override\n");
		goto out;
	}

	own = kzalloc(64, GFP_KERNEL);
	if (own == NULL) {
		printk("Failed to allocate buffer in chown_override\n");
		goto out;
	}

	grp = kzalloc(64, GFP_KERNEL);
	if (grp == NULL) {
		printk("Failed to allocate buffer in chown_override\n");
		goto out;
	}

	if (pathname != NULL)
		l1 = strlen(pathname);
	tot = l1 + 1024;
	com = kzalloc(tot, GFP_KERNEL);
	if (com == NULL) {
		printk("Failed to allocate buffer in symlink_override\n");
		goto out;
	}

	sprintf(errs, "%d", err);
	sprintf(own, "%d", owner);
	sprintf(grp, "%d", group);
	strcat(com, "Name : ");
	strcat(com, pathname);
	strcat(com, " owner ID : ");
	strcat(com, own);
	strcat(com, ", group ID:");
	strcat(com, grp);
	strcat(com, " return Value : ");
	strcat(com, errs);
	strcat(com, "\n");
	set_fs(KERNEL_DS);
	vfs_write(chown_log, com, strlen(com), &chown_log->f_pos);
	set_fs(oldfs);

	if (com != NULL)
		kfree(com);
	if (errs != NULL)
		kfree(errs);
	if (own != NULL)
		kfree(own);
	if (grp != NULL)
		kfree(grp);
out:
	return err;
}


asmlinkage int chmod_override(const char *pathname, mode_t mode)
{
	pid_t by;
	char *com, *pid1, *mode1, *errs;
	int err;
	mm_segment_t oldfs;
	int l1, tot;

	by = task_tgid_vnr(current);
	oldfs = get_fs();
	l1 = 0;
	chmod_orig = get_func_ptr(orig_head, __NR_chmod);
	err = chmod_orig(pathname, mode);

	pid1 = kzalloc(64, GFP_KERNEL);
	if (pid1 == NULL) {
		printk("Failed to allocate buffer in chmod_override\n");
		goto out;
	}

	errs = kzalloc(64, GFP_KERNEL);
	if (errs == NULL) {
		printk("Failed to allocate buffer in chmod_override\n");
		goto out;
	}

	mode1 = kzalloc(64, GFP_KERNEL);
	if (mode1 == NULL) {
		printk("Failed to allocate buffer in chmod_override\n");
		goto out;
	}

	if (pathname != NULL)
		l1 = strlen(pathname);

	tot = l1 + 1024;
	com = kzalloc(tot, GFP_KERNEL);
	if (com == NULL) {
		printk("Failed to allocate buffer in symlink_override\n");
		goto out;
	}

	sprintf(pid1, "%d", by);
	sprintf(errs, "%d", err);
	sprintf(mode1, "%d", mode);
	strcat(com, "pid : ");
	strcat(com, pid1);
	strcat(com, " Filename: ");
	strcat(com, pathname);
	strcat(com, " Permissions: ");
	strcat(com, mode1);
	strcat(com, " Return Value: ");
	strcat(com, errs);
	strcat(com, "\n");
	set_fs(KERNEL_DS);
	vfs_write(chmod_log, com, strlen(com), &chmod_log->f_pos);
	set_fs(oldfs);

	if (pid1 != NULL)
		kfree(pid1);
	if (com != NULL)
		kfree(com);
	if (errs != NULL)
		kfree(errs);
	if (mode1 != NULL)
		kfree(mode1);

out:
	return err;
}


asmlinkage int symlink_override(const char *name1, const char *name2)
{
	pid_t by;
	char *com, *pid1, *errs;
	int ret;
	mm_segment_t oldfs;
	int l1, l2, tot;

	by = task_tgid_vnr(current);
	oldfs = get_fs();
	l1 = 0;
	l2 = 0;
	ret = 0;
	symlink_orig = get_func_ptr(orig_head, __NR_symlink);
	ret = symlink_orig(name1, name2);

	pid1 = kzalloc(64, GFP_KERNEL);
	if (pid1 == NULL) {
		printk("Failed to allocate buffer in symlink_override\n");
		goto out;
	}

	errs = kzalloc(64, GFP_KERNEL);
	if (errs == NULL) {
		printk("Failed to allocate buffer in symlink_override\n");
		goto out;
	}

	if (name1 != NULL)
		l1 = strlen(name1);
	if (name2 != NULL)
		l2 = strlen(name2);

	tot = l1 + l2 + 1024;
	com = kzalloc(tot, GFP_KERNEL);
	if (com == NULL) {
		printk("Failed to allocate buffer in symlink_override\n");
		goto out;
	}

	sprintf(pid1, "%d", by);
	sprintf(errs, "%d", ret);
	strcat(com, "pid : ");
	strcat(com, pid1);
	strcat(com, " FileName: ");
	strcat(com, name1);
	strcat(com, " symlink: ");
	strcat(com, name2);
	strcat(com, " Return Value: ");
	strcat(com, errs);
	strcat(com, "\n");
	set_fs(KERNEL_DS);
	vfs_write(symlink_log, com, strlen(com), &symlink_log->f_pos);
	set_fs(oldfs);
	if (pid1 != NULL)
		kfree(pid1);
	if (com != NULL)
		kfree(com);
	if (errs != NULL)
		kfree(errs);

out:
	return ret;
}

asmlinkage int kill_override(pid_t pid, int sig)
{
	int err;
	/* write record to the file and call the original system call */
	pid_t by;
	char *pid1, *com, *pid2, *signal;
	mm_segment_t oldfs;

	com = NULL;
	pid2 = NULL;
	pid1 = NULL;
	signal = NULL;
	oldfs = get_fs();
	by = task_tgid_vnr(current);
	printk("In kill override \n");
	pid1 = kzalloc(64, GFP_KERNEL);
	if (pid1 == NULL) {
		printk("Failed to allocate buffer in kill_override\n");
		goto out;
	}

	pid2 = kzalloc(64, GFP_KERNEL);
	if (pid2 == NULL) {
		printk("Failed to allocate buffer in kill_override\n");
		goto out;
	}

	signal = kzalloc(128, GFP_KERNEL);
	if (signal == NULL) {
		printk("Failed to allocate buffer in kill_override\n");
		goto out;
	}

	com = kzalloc(512, GFP_KERNEL);
	if (com == NULL) {
		printk("Failed to allocate buffer in kill_override\n");
		goto out;
	}

	sprintf(pid1, "%d", by);
	strcat(com, pid1);
	strcat(com, " issued ");
	sprintf(pid2, "%d", pid);
	sprintf(signal, "%d", sig);
	strcat(com, signal);
	strcat(com, " signal to ");
	strcat(com, pid2);
	strcat(com, "\n");
	printk("recording : %s \n", com);
	set_fs(KERNEL_DS);
	vfs_write(kill_log, com, strlen(com), &kill_log->f_pos);
	set_fs(oldfs);
	if (pid1 != NULL)
		kfree(pid1);
	if (pid2 != NULL)
		kfree(pid2);
	if (signal != NULL)
		kfree(signal);
	if (com != NULL)
		kfree(com);

out:
	/* now call the original sys call */
	kill_orig = get_func_ptr(orig_head, __NR_kill);

	err = kill_orig(pid, sig);
	return err;

}

struct new_syscall *create_vector(int syscall_num, void *addr)
{
	struct new_syscall *s_node1;

	s_node1 = kmalloc(sizeof(struct new_syscall), GFP_KERNEL);
	if (IS_ERR(s_node1)) {
		printk("Error during kmalloc\n");
		return s_node1;
	}
	s_node1->next = NULL;
	s_node1->syscall_num = syscall_num;
	s_node1->syscall_addr = addr;
	return s_node1;
}

static int __init v2_start(void)
{
	int err = 0;
	struct new_syscall *vec_addr2, *vec_addr3, *vec_addr4, *vec_addr5;
	kill_log = filp_open("kill_.log", O_CREAT|O_WRONLY, 0);
	if (!kill_log || IS_ERR(kill_log)) {
		printk("error opening the file kill_.log; Cannot register the sys_call_vector \n");
		err = EBADFD;
		goto out;
	}

	clone2_log = filp_open("clone2_.log", O_CREAT|O_WRONLY, 0);
	if (!clone2_log || IS_ERR(clone2_log)) {
		printk("error opening the file clone2_.log; Cannot register the sys_call_vector \n");
		err = EBADFD;
		goto out;
	}

	symlink_log = filp_open("symlink_.log", O_CREAT|O_WRONLY, 0);
	if (!symlink_log || IS_ERR(symlink_log)) {
		printk("error opening the file symlink_.log; Cannot register the sys_call_vector \n");
		err = EBADFD;
		goto out;
	}

	chmod_log = filp_open("chmod_.log", O_CREAT|O_WRONLY, 0);
	if (!chmod_log || IS_ERR(chmod_log)) {
		printk("error opening the file chmod_.log; Cannot register the sys_call_vector \n");
		err = EBADFD;
		goto out;
	}

	chown_log = filp_open("chown_.log", O_CREAT|O_WRONLY, 0);
	if (!chown_log || IS_ERR(chown_log)) {
		printk("error opening the file chown_.log; Cannot register the sys_call_vector \n");
		err = EBADFD;
		goto out;
	}


	/* new syscall vector creation */
	vec = kmalloc(sizeof(struct syscall_vector), GFP_KERNEL);
	if (IS_ERR(vec)) {
		err = PTR_ERR(vec);
		goto out;
	}
	vec->next = NULL;
	vec->owner = THIS_MODULE;
	vec->rc = 0;
	vec_addr = create_vector(__NR_kill, kill_override);

	vec->name = "vector2";
	if (IS_ERR(vec_addr)) {
		err = PTR_ERR(vec_addr);
		goto out;
	}
	vec->new_sys_calls = vec_addr;
	vec_addr2 = create_vector(__NR_clone2, clone2_override);
	if (IS_ERR(vec_addr2)) {
		err = PTR_ERR(vec_addr2);
		goto out;
	}
	vec_addr->next = vec_addr2;

	vec_addr3 = create_vector(__NR_symlink, symlink_override);
	if (IS_ERR(vec_addr3)) {
		err = PTR_ERR(vec_addr3);
		goto out;
	}
	vec_addr2->next = vec_addr3;

	vec_addr4 = create_vector(__NR_chmod, chmod_override);
	if (IS_ERR(vec_addr4)) {
		err = PTR_ERR(vec_addr4);
		goto out;
	}
	vec_addr3->next = vec_addr4;

	vec_addr5 = create_vector(__NR_chown, chown_override);
	if (IS_ERR(vec_addr5)) {
		err = PTR_ERR(vec_addr5);
		goto out;
	}
	vec_addr4->next = vec_addr5;


	printk("before registering num_nodes : %d\n", num_vectors());
	err = register_syscall_vector(vec);
	printk("after registering num_nodes : %d\n", num_vectors());
	if (err < 0) {
		printk("Can not register v2\n");
		goto out;
	}
	printk("vector id : %d\n", err);
	err = 0;
out:
	return err;
}

/*
 * unlink the output file when error occurs
 */
void unlink_file(struct file *file)
{
	struct dentry *d1;
	struct inode *dir;

	d1 = file->f_path.dentry;
	dir = file->f_path.dentry->d_parent->d_inode;
	/*close the file before unlinking*/
	filp_close(file, NULL);
	vfs_unlink(dir, d1, NULL);
}

static void __exit v2_end(void)
{
	printk("before unregistering num_nodes : %d\n", num_vectors());
	unregister_syscall_vector(vec);
	printk("after unregistering num_nodes : %d\n", num_vectors());
	/* close the file */
	if (kill_log != NULL)
		unlink_file(kill_log);
	if (clone2_log != NULL)
		unlink_file(clone2_log);
	if (symlink_log != NULL)
		unlink_file(symlink_log);
	if (chmod_log != NULL)
		unlink_file(chmod_log);
	if (chown_log != NULL)
		unlink_file(chown_log);
	printk("vector 2 module is removed\n");
}
module_init(v2_start);
module_exit(v2_end);

MODULE_LICENSE("GPL");
