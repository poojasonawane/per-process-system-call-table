#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/syscalls.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/register.h>

struct syscall_vector *vec;
struct new_syscall *create_vector(void);
struct new_syscall *vec_addr;

typedef asmlinkage long (*mkdir_ptr) (const char __user *pathname, umode_t mode);
asmlinkage long (*mkdir_orig)(const char __user *pathname, umode_t mode);
asmlinkage long mkdir_override(const char __user *pathname, umode_t mode);

typedef asmlinkage long (*read_ptr) (unsigned int fd, char __user *buf, size_t count);

asmlinkage long (*read_orig)(unsigned int fd, char __user *buf, size_t count);
asmlinkage long read_override(unsigned int fd, char __user *buf, size_t count);

typedef asmlinkage long (*write_ptr) (unsigned int fd, char __user *buf, size_t count);

asmlinkage long (*write_orig)(unsigned int fd, char __user *buf, size_t count);
asmlinkage long write_override(unsigned int fd, char __user *buf, size_t count);

extern struct new_syscall *orig_head;

/*
 * Read syscall override
 */
void temp_f(struct new_syscall *head)
{
	struct new_syscall *temp;

	for (temp = head; temp; temp = temp->next) {
		printk("syscall number : %d\n", temp->syscall_num);
	}
	return;
}


asmlinkage long read_override(unsigned int fd, char __user *buf, size_t count)
{
	int err = 0;
	int c = 0;
	char *content;
	mm_segment_t oldfs;

	content = (char *)kmalloc(count, GFP_KERNEL);
	if (IS_ERR(content)) {
		pr_info("kmalloc failed in decryption\n");
		goto out;
	}
	read_orig = get_func_ptr(orig_head, __NR_read);
	oldfs = get_fs();
	set_fs(KERNEL_DS);
	err = read_orig(fd, content, count);
	set_fs(oldfs);
	/*pr_info("original_read content bytes read: %d, count: %d\n", err, count);

	//pr_info("In read_override, content to be decrypted %s\n", content);*/
	while (c < count) {
		content[c] -= 3;
		c++;
	}
	memcpy(buf, content, count);
	/*pr_info("Decryted buf %s, %d\n", buf, c);*/
out:
	if (content != NULL)
		kfree(content);
	return err;
}
/*
 * Write syscall override
 */

asmlinkage long write_override(unsigned int fd, char __user *buf, size_t count)
{
	int err = 0;
	int c = 0;
	char *content;
	mm_segment_t oldfs;

	content = (char *)kmalloc(count, GFP_KERNEL);
	if (IS_ERR(content)) {
		pr_info("kmalloc failed in encryption\n");
		goto out;
	}
	memcpy(content, buf, count);
	/*pr_info("In write_override, content to be encrypted %s\n", content);*/
	if (fd != 1) {
		/*printk("Encrypting here\n");*/
		while (c < count) {
			content[c] += 3;
			c++;
		}
	}
	/*pr_info("Encryted content: %s, size: %d\n", content, c);*/
	write_orig = get_func_ptr(orig_head, __NR_write);
	oldfs = get_fs();
	set_fs(KERNEL_DS);

	err = write_orig(fd, content, count);
	set_fs(oldfs);
	/*pr_info("Ercypted content bytes %d\n", err);*/
out:
	if (content != NULL)
		kfree(content);
	return err;
}

/*
 * mkdir syscall override
 */
asmlinkage long mkdir_override(const char __user *pathname, umode_t mode)
{
	struct task_struct *task = current;
	int err;
	char *dir;
	mkdir_ptr mkdir_orig;
	pr_info("Process name: %s\n", task->comm);
	dir = task->comm;
	if (strcmp(dir, "a.out") == 0) {
		err = -EPERM;
		goto out;
	}
	mkdir_orig = get_func_ptr(orig_head, __NR_mkdir);
	/*printk("v3: mkdir_override : vector id : %d\n", task->vector_id);*/
	//print_new_syscalls(task->vector_id);
	temp_f(orig_head);
	if (!mkdir_orig) {
		err = -1;
		printk("Function pointer is NULL\n");
		goto out;
	}
	pr_info("After mkdir_orig\n");
	err = mkdir_orig(pathname, mode);
out:
	return err;
}
struct new_syscall *create_vector(void)
{
	struct new_syscall *s_node1;
	struct new_syscall *s_node2;
	struct new_syscall *s_node3;
	s_node1 = kmalloc(sizeof(struct new_syscall), GFP_KERNEL);
	if (IS_ERR(s_node1)) {
		pr_info("Cannot kmalloc in s_node1\n");
		return s_node1;
	}
	s_node1->next = NULL;
	s_node1->syscall_num = __NR_mkdir;
	s_node1->syscall_addr = mkdir_override;

	s_node2 = kmalloc(sizeof(struct new_syscall), GFP_KERNEL);
	if (IS_ERR(s_node2)) {
		pr_info("Cannot kmalloc in s_node2\n");
		return s_node2;
	}
	s_node2->next = NULL;
	s_node2->syscall_num = __NR_read;
	s_node2->syscall_addr = read_override;
	s_node1->next = s_node2;

	s_node3 = kmalloc(sizeof(struct new_syscall), GFP_KERNEL);
	if (IS_ERR(s_node3)) {
		pr_info("Cannot kmalloc in s_node3\n");
		return s_node3;
	}
	s_node3->next = NULL;
	s_node3->syscall_num = __NR_write;
	s_node3->syscall_addr = write_override;
	s_node2->next = s_node3;
	return s_node1;
}
static int __init v3_start(void)
{
	int err = 0;
	vec = kmalloc(sizeof(struct syscall_vector), GFP_KERNEL);
	if (IS_ERR(vec)) {
		err = PTR_ERR(vec);
		goto out;
	}
	vec->next = NULL;
	vec->owner = THIS_MODULE;
	vec->rc = 0;
	vec->name = "Vector3";
	vec_addr = create_vector();
	if (IS_ERR(vec_addr)) {
		err = PTR_ERR(vec_addr);
		goto out;
	}
	vec->new_sys_calls = vec_addr;
	pr_info("Before Module v3 added: %d\n", num_vectors());
	err = register_syscall_vector(vec);
	//print_new_syscalls(err);
	pr_info("After Module v3 added: %d\n", num_vectors());
	if (err < 0) {
		pr_info("Cannot register Module v3\n");
		goto out;
	}
	pr_info("vector id: %d\n", err);
	err = 0;
out:
	return err;

}
static void __exit v3_end(void)
{
	pr_info("Before Module v3 removed: %d\n", num_vectors());
	unregister_syscall_vector(vec);
	pr_info("After Module v3 removed: %d\n", num_vectors());
}
module_init(v3_start);
module_exit(v3_end);
MODULE_LICENSE("GPL");

