TEAM MEMBERS:
---------------
	1) Krishna Chaitanya Chakka - 110347217
	2) Meghana Doppalapudi - 110739940
	3) Pooja Sonawane - 110739621
	4) Pranathi Irala - 110754921

OVERVIEW:
--------
	A loadable kernel module cannot modify the global system call table. However, it would be useful having different syscalls per process. In this assignment, we design and implement linux kernel-based system to support per process system call vectors.

	- Created each module for each system call vector
	- 

DESIGN:

DEFINITIONS:
-----------
	Default vector ID: 0
		default vector is the global system call vector.

Clone system call modifications:
------------------------------
	clone2:
	------
		We added a new system call clone2, which supports additional parameter "vector_id". 

	We added a flag "CLONE_SYSCALLS" in clone and clone2. 
		If "CLONE_SYSCALLS" is set, then the child inherits the vector_id from parent. 
		If "CLONE_SYSCALLS" is not set
			- clone sets the child's vector_id with default vector_id. 
			- clone2 sets the child's vector_id with the vector_id passed as a parameter.

	For implementing the above functionality, we modified the definition for _do_fork. We have added the extra parameter(vector_id).

Modifying task_struct:
--------------------
	The properties for each process is defined in task_struct. We have customized the execution of system calls for each process, by adding a "vector_id" variable in its task_struct. 


Overriding global syscall table:
-------------------------------
	We have a module which overrides the desired system calls

	We defined three different verions of system calls.
	1) Original system call (denote : orig_syscall_xxx) (eg: orig_syscall_read for read system call)
		This contains the function pointer for the original system call.

	2) Redirect system call (denote : redirect_syscall_xxx) (eg: redirect_syscall_read for read system call)
		This code redirects the process which system call to execute based on vector id

	3) New system call      (denote : new_syscall_xxx)(eg : new_syscall_read for read system call)
		This contains the implementation of new system call. (new modifications + original system call)

Original global system call vector:
---------------------------------
	Global system call vector is an array of function pointers indexed by the corresponding system call number.

	Initially the state of global system call vector is as follows:
		________________________________________________________________________________
		.......	|orig_syscall_read | orig_syscall_write | orig_syscall_open | ..........
		________|__________________|____________________|___________________|____________

Modifying global syscall vector:
--------------------------------
	We override the orig_syscall_xxx function pointers with redirect_syscall_xxxx function pointers.

	Now the state of the global system call vector is as follows
		__________________________________________________________________________________________
		.......	|redirect_syscall_read | redirect_syscall_write | redirect_syscall_open | ..........
		________|______________________|________________________|_______________________|_________

	Redirecting system call based on vector ID:
	-------------------------------------------
		1) Retrieve the current system call vector ID from the process task_struct
		2) Get new system call address (written in the corresponding system call vector)
		3) Call this new system call with the given parameters.

	redirect_syscall_xxxx pseduo code:
	---------------------------------	
			asmlinkage long redirect_syscall_xxx(args) {
				
				vector_id = get_vector_id_for_current_process();
				new_syscall_xxx = get_new_syscall_address(vector_id);
				return new_syscall_xxx(args);
			}

	Since we have overriden the orig_syscall_xxxx with redirect_syscall_xxxx, we save the orig_syscall_xxxx in a linkedlist.

	-------------
	|Orig head   |---
	-------------   |
			|____\ __________________        __________________       __________________
			     /|orig_syscall_read |____\ |orig_syscall_write|___\ |orig_syscall_open |___
			      |_________________ |    / |__________________|   / |__________________|   |
			      								 	        |
												     ------
												      ----
		Orig_head stores all the original system calls implementations and it is exported.

Structure of system call vector:
--------------------------------
	Each system call vector is a loadable module. Each system call vector contains the following attributes.
		- ID
		- Vector name
		- new_syscalls_xxx

	new_syscalls_xxxx for a vector ID is a linked list for new_syscall_xxxx(new system call implementation for a vector)

	syscall_vector1:
	 _________________
	|ID               |
	|vector_name      |
	|new_syscalls_xxx |------
	 ________________ |	|
				|____\ __________________       __________________      _________________
				     /|new_syscall_read |____\ |new_syscall_write|___\ |new_syscall_open |___
				      |_________________|    / |_________________|   / |_________________|   |
				      								 	     |
													   ------
													    ----

	
Registration and Unregistration:
-------------------------------
	We have followed the filesystem registration design for registering the system call vectors. We maintained a linkedlist of all the registered system call vectors and we exposed methods for registration and unregistration of the system call vectors. 

	 ---------------------
	|syscall_vectors_head |------
	 --------------------- 	    |
				    |____\ __________________         __________________      _________________
				         /|syscall_vector1   | ____\ |syscall_vector2  |___\ |syscall_vector3  |___
				          |__________________|     / |_________________|   / |_________________|   |
				      	    							 	           |
													         ------
													          ----


	
	- We maintain refcnt for each system call vector. Whenever a process is assigned a particular system call vector, we decrement the previous system call vector refcnt and increment the new system call vector refcnt.    
	- A system call vector can not be unregistered, if some process is refering to that system call vector.
	- We do not maintain the default system call vector

New system call implementations for  each system call vector:
------------------------------------------------------------
Vector1:
-------
	We have overriden the following system calls in vector1:
		1) unlink
		2) open
	
	Unlink:
	------
		If a file has an extension ".protect", then we cannot unlink that file. 
	
	Open:
	----
		If the content of a file has pattern "virus", then the process can not open the file.

Vector2:
-------
	We have overriden the following system calls in vector2:
		1) kill
		2) clone
		3) symlink
		4) chmod
		5) chown

	For every system call, we log the arguments and return value;

Vector3:
-------
	We have overridden the following system calls in vector3:
		1) read
		2) write
		3) mkdir
	
	We have implemented "Caesar Cipher" for write and read system calls.

	Read:
	----
		We read from an encrypted file, and decrypt using caesar cipher.

	Write:
	-----
		For the content of a file, we encrypt and write the encrypted content to a file using caesar cipher.

	mkdir:
	-----
		A process with name "a.out"(executable from gcc), can not create directories.

Implementation of IOCTL module:
-------------------------------
	This module is reponsible for implementing ioctl(2) that changes the syscall vector used by a process that is running.
	A character device is used to transfer data to and from application. We have created a device file entry by running the following command:
	
	mknod <device entry> <device type> <major number> <minor number>
	mknod /dev/ioctl_device c 121 122
	Here, device number is 121 (number chosen randomly) and device name is ioctl_device

	We register character device ioctl_device when the ioctl_module is inserted i.e., register_chrdev is called when the module is inserted.
	The file_operations data structure defined in /linux/fs has pointers to functions that define the behavior of file operations. 
	We have provided unlocked_ioctl which is called by ioctl system call.

	struct file_operations fops = {
		.unlocked_ioctl = device_ioctl;
	}

	We have two ioctls which both take pointer to a structure ioctl_packet defined in ioctl_header.h
	struct ioctl_packet {
		int pid;
		int vid;
		char buff[SIZE];
		int size;
	};

	1) IOCTL_SET_VECTOR :
   		This ioctl sets the vector id of given process (given as pid in ioctl_packet). This vector id is set in struct task_struct of the process.
		We then increment the reference count of that vector and decrement the vector previously used by the process.
	 	We have used find_get_pid and pid_task functions defined in /linux/pid.h and /linux/pid_namespace.h> to get task struct of process given its pid.
   
	2) IOCTL_SHOW_VECTORS :
		This ioctl shows all the vectors currently registered. It displays vector information like Vector Name, Vector Id, Vector reference count.
		When process id is given, IOCTL_SHOW_VECTORS shows vector information of a single vector.
   
	We unregister the character device when the module is unloaded.

Files Added/ Modified:
---------------------
	Modified:
	--------
	kernel/fork.c
	include/linux/sched.h
	arch/x86/entry/syscalls/syscall_64.tbl

	Newly Added:
	-----------
	include/linux/register.h
	kernel/register.c
	hw3/
	hw3/ioctl_module/
	hw3/ioctl_module/ioctl_header.h
	hw3/ioctl_module/ioctl_module.c
	hw3/syscall_override/
	hw3/syscall_override/syscall_override.h
	hw3/syscall_override/syscall_override.c
	hw3/v1/
	hw3/v1/v1.c
	hw3/v2/
	hw3/v2/v2.c
	hw3/v3/
	hw3/v3/v3.c
	hw3/test/
	hw3/Makefile
	hw3/script.sh

How to build?
-------------

	make
	make modules_install
	make install
	sh hw3/script.sh

	script.sh:
	---------
	This script cleans and builds hw3/ folder and removes (if already inserted) modules. Also, inserts the built modules.
	It also runs mknod command to create character device entry
	
How to test?
--------------
	We have provided scripts ( /hw3/test folder) for testing the functionality.

	test_show_set_show_vid.c 
	------------------------
	- This program shows the vector information of a given process before and after setting it to another vector.
	- ./test_show_set_show_vid <pid> <new_vid>

	test_show_all_vid.c
	-------------------
	- This program shows all the vector information of vectors which are currently registered
	- ./test_show_all_vid

	test_clone2.c (clone2 Demo)
	--------------------------
	- This program clones a child process and assigns vector id ( inherit / new vector ) based on CLONE_SYSCALLS.
	- ./test_clone2 <y/n> <child_vid>
	- Here, y/n shows whether CLONE_SYSCALLS flag is set or not

	test_clone.c (clone)
	--------------------
	- This program clones a child process and assigns vector id ( inherited / default ) based on CLONE_SYSCALLS.
	- ./test_clone <y/n>
	- Here, y/n shows whether CLONE_SYSCALLS flag is set or not

	test_mkdir.c
	------------
	- This program demonstrates overridden mkdir syscall.
	- ./test_mkdir <vid> <dir_to_created>

	test_crypt.c
	------------
	- This program demonstrates overridden read and write syscalls.
	- We encrypt and write to the file and later decrypt to show to the user.
	- ./test_crypt <vid> <file_with_encrypted_content>

	test_v2_clone.c
	---------------
	- This program assigns vector to current process,then clones another process. 
	- The overriden clone system call logs this action in clone_.log file in hw3 folder.
	- ./XXX <vid>

	test_kill.c
	------------
	- This program assigns vector to current process,then kills the process with SIGKILL signal.
	- The overriden kill system call logs this action in kill_.log file in hw3 folder.
	- ./XXX <vid> <pid of process to kill> 

	test_v2_chown.c
	---------------
	- This program assigns vector to current process,then calls chown() on the arguments passed.
	- The overriden chown system call logs this action in chown_.log file in hw3 folder.
	- ./XXX <vid> <username> <path>

 	test_v2_chmod.c
	---------------
	- This program assigns vector to current process,then calls chmod() on the arguments passed.
	- The overriden chmod system call logs this action in chmod_.log file in hw3 folder.
	- ./XXX <vid> <filename> <mode>

 	test_v2_symlink.c
	-----------------
	- This program assigns vector to current process,then calls symlink() on the arguments passed.
	- The overriden symlink system call logs this action in symlink_.log file in hw3 folder.
	- ./XXX <vid> <filename> <symlink>

References:
--------------
	http://derekmolloy.ie/writing-a-linux-kernel-module-part-2-a-character-device/
	http://stackoverflow.com/questions/26000691/system-call-interception-via-loadable-kernel-module
	Linux Kernel Development by Robert Love
	http://lxr.free-electrons.com/

