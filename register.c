#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/printk.h>
#include <linux/syscalls.h>
#include <linux/slab.h>
#include <linux/init.h>
#include <linux/register.h>

static struct syscall_vector *head;
static struct syscall_vector *tail;
struct mutex sysv_lock;

int add_vector_to_list(struct syscall_vector *vector_addr);
struct syscall_vector *remove_vector_from_list(struct syscall_vector *vector_addr);

struct syscall_vector *get_syscall_vector_head(void)
{
	return head;
}
EXPORT_SYMBOL(get_syscall_vector_head);

struct syscall_vector *get_syscall_vector(int v_id)
{
	struct syscall_vector *temp;
	for (temp = head; temp; temp = temp->next) {
		if (temp->id == v_id) {
			return temp;
		}
	}
	return NULL;
}
EXPORT_SYMBOL(get_syscall_vector);

void print_new_syscalls(int v_id)
{
	struct syscall_vector *t;
	struct new_syscall *temp;
	t = get_syscall_vector(v_id);
	for (temp = t->new_sys_calls; temp; temp = temp->next) {
		printk("\t\tsyscall num : %d\n", temp->syscall_num);
	}
	return;
}
EXPORT_SYMBOL(print_new_syscalls);

struct syscall_vector *get_syscall_vector_name(char *name)
{
	struct syscall_vector *temp;
	for (temp = head; temp; temp = temp->next) {
		if (!strcmp(temp->name, name)) {
			return temp;
		}
	}
	return NULL;
}
EXPORT_SYMBOL(get_syscall_vector_name);


void *get_func_ptr(struct new_syscall *vec, int syscall_num)
{
	struct new_syscall *temp;
	void *syscall_addr;

	syscall_addr = NULL;
	if (!vec) {
		printk("No system calls in the table\n");
		goto out;
	 }
	temp = vec;
	while (temp != NULL) {
		if (temp->syscall_num == syscall_num) {
			syscall_addr = temp->syscall_addr;
			goto out;
		}
		temp = temp->next;
	}
out:
	return syscall_addr;
}
EXPORT_SYMBOL(get_func_ptr);


int add_vector_to_list(struct syscall_vector *vector)
{
	int v_id;

	printk("add_vector_to_list : Vector address to be registered : %lu\n", (unsigned long) vector);
	/*  Dont add default syscall vector.
	 *  So start with vector id = 1
	 */
	if (head == NULL) {
		printk("add_vector_to_list : head is NULL \n");
		head = vector;
		tail = head;
		head->id = 1;
		v_id = 1;
	} else {
		printk("add_vector_to_list : head is not null\n");
		v_id = tail->id + 1;
		printk("vid here : %d \n",v_id);
		tail->next = vector;
		tail = tail->next;
		printk("tail vector name: %s  \n",tail->name);
		tail->id = v_id;
		printk("tail v_id : %d \n",tail->id);
	}
/*out: */
	return v_id;
}

void free_linked_list(struct syscall_vector *vector) {
	struct new_syscall *head;
	struct new_syscall *temp1;

	head = vector->new_sys_calls;
	temp1 = vector->new_sys_calls;
	while (head != NULL) {
		temp1 = head;
		head = head->next;
		kfree(temp1);
	}
	kfree(vector);
	return;
}


struct syscall_vector *remove_vector_from_list(struct syscall_vector *vector)
{
	int rc;
	struct syscall_vector *temp = head;
	struct syscall_vector *curr = NULL;
	struct syscall_vector *prev = NULL;
	struct syscall_vector *ret = NULL;

	printk("remove_vector_from_list : vector address to be removed : %lu\n", (unsigned long) vector);

	while (temp) {
		printk("\t node address : %lu\n", (unsigned long) temp);
		if (temp == vector) {
			curr = temp;
			rc = curr->rc;
			goto rc_count;
		} else {
			prev = temp;
			temp = temp->next;
		}
	}


	if (!temp && !curr) {
		printk("Remove operation failed. Vector not registered\n");
		ret = ERR_PTR(-EINVAL);
		goto out;
	}

rc_count:
	if (rc > 0) {
		printk("Remove operation failed. Vector still in use\n");
		ret = ERR_PTR(-EBUSY); /* TODO: check error */
		goto out;
	} else {
		ret = curr;
		if (head == tail) {
			head = NULL;
			tail = NULL;
		} else if (prev == NULL) {
			head = curr->next;
		} else {
			prev->next = curr->next;
			if (tail == curr)
				tail = prev;
		}
		curr->next = NULL;
		free_linked_list(curr);
	}

out:
	return ret;
}


int register_syscall_vector(struct syscall_vector *vector)
{
	int v_id;
	mutex_lock(&sysv_lock);
	v_id = add_vector_to_list(vector);
	mutex_unlock(&sysv_lock);

	return v_id;
}
EXPORT_SYMBOL(register_syscall_vector);

struct syscall_vector *unregister_syscall_vector(struct syscall_vector *vector)
{
	struct syscall_vector *v;
	mutex_lock(&sysv_lock);
	v = remove_vector_from_list(vector);
	mutex_unlock(&sysv_lock);

	/* TODO : free if rc_count == 0 */
	return v;
}
EXPORT_SYMBOL(unregister_syscall_vector);

int num_vectors(void)
{
	struct syscall_vector *temp = head;
	int count;

	count = 0;
	while (temp) {
		count += 1;
		temp = temp->next;
	}
	return count;
}
EXPORT_SYMBOL(num_vectors);

static int __init register_start(void)
{
	int err = 0;
	/*struct new_syscall *addr;
	addr = NULL;*/
	mutex_init(&sysv_lock);
	/*get global system call vector
	addr = (unsigned long) get_syscall_tbl();
	printk("module installed : %lu\n", addr);
	err = register_syscall_vector(addr);
	*/
	printk("Unique id for default syscall vector : %d\n", err);
	return err;
}
/*
static void __exit register_end(void)
{
	if (head->next == NULL) {
		kfree(head);
	}
	printk("register module is removed\n");
}
*/

module_init(register_start);
/*
module_exit(register_end);
MODULE_LICENSE("GPL"); */
